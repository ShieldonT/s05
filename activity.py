from abc import ABC, abstractclassmethod
class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    def make_sound(self,):
        pass
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
    # setter
    def set_cat_name(self,name):
        self._name = name
    def set_cat_breed(self,breed):
        self._breed = breed
    def set_cat_age(self,age):
        self._age = age
    # getter
    def get_cat_name(self):
        print(f"The cats name is {self._name}")
    def get_cat_breed(self):
        print(f"The cats breed is {self._breed}")
    def get_cat_age(self):
        print(f"The cats age is {self._age}")
    # method
    def eat(self, food):
        print(f"Serve me {food}")
    def make_sound(self,):
        print("Miaow! Nyaw! Nyaaaa!")
    def call(self,):
        print(f"{self._name}, come on!")
class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
    # setter
    def set_cat_name(self,name):
        self._name = name
    def set_cat_breed(self,breed):
        self._breed = breed
    def set_cat_age(self,age):
        self._age = age
    # getter
    def get_cat_name(self):
        print(f"The dogs name is {self._name}")
    def get_cat_breed(self):
        print(f"The dogs breed is {self._breed}")
    def get_cat_age(self):
        print(f"The dogs age is {self._age}")
    # method
    def eat(self, food):
        print(f"Eaten {food}")
    def make_sound(self,):
        print("Bark! Woof! Arf!")
    def call(self,):
        print(f"Here {self._name}!") 
# new instance
cat1 = Cat("Evelyn","Ragdoll",5)
dog1 = Dog("Tobi","Shih Tzu",3)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()


    
    